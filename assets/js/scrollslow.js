jQuery(document).ready(function($){
    //Dans le HREF je rentre l'ID de la ou je veux que cela m'emmene au moment ou je clique sur mon bouton accueil qui à comme HREF"L'ID LesNews"
    $(document).on('click', 'a[href^="#LesNews"]', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });
});