<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
<?php require 'connexionbdd.php' ?>
<?php session_start(); ?>
</head>
<body>
<main id="brigade">
<section class="back">

<?php require 'menu.php'; ?>

<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x align-center">
        <div class="cell small-12 medium-12 large-12 decale">
            <h1>Les 3 brigades de l'Attaque des Titans</h1>
        </div>
    </div>
</div>
<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x">
        <div class="cell small-4 contour" align="center">
            <h3>Le Bataillon d'exploration</h3>
            <img src="assets/images/bataillon_exp.png" alt="Bataillon d'exploration">
            <p class="decale">C'est une section spécialisée dans les enquêtes, patrouilles et expéditions extérieures des Murs à propos des Titans, de leur origine à l'étude des possibilités de leur extermination. Le chef étant : Erwin Smith. Elle est de ce fait considérée comme étant une unité suicidaire puisque les êtres humains sont supposés être « impuissants » face aux Titans, et qu'une moyenne de 30% des effectifs périssent lors des opérations.</p>
        </div>
        <div class="cell small-4 contour" align="center">
            <h3>Les Brigades Spéciales</h3>
            <img class="misehaut"src="assets/images/brig_spe.png" alt="Brigade spécial">
            <p class="decale">C'est une section spécialisée dans la protection des autorités royales (dont le Souverain lui-même). Elle est réservée aux dix meilleurs soldats de chaque Brigade d'Entraînement, à condition que ces derniers veuillent la rejoindre. Le chef étant : Le Roi. Elle a pour objectif de contrôler l'ordre à l'intérieur des Murs, elle effectue le rôle de la police militaire.</p>
        </div>
        <div class="cell small-4 contour" align="center">
            <h3>La Garnison</h3>
            <img class="alignement" src="assets/images/garnison.png" alt="Garnison">
            <p class="decale2">C'est une section spécialisée dans la protection des villes, notamment des district, et du renforcement des Murs. Elle a pour objectif d'assurer la protection intérieure aussi bien des biens matériels que des vies humaines. Le chef étant : Dot Pixis. En tant que tels, ils passent la plupart de leur temps à se préparer pour toute éventuelle attaque titanesque contre le mur. La garnison s'occupe des canons sur le mur, souvent utilisés pour la défense à long terme contre la menace continuelle des Titans.</p>
        </div>
    </div>
</div>

</section>
</main>
</body>
</html>