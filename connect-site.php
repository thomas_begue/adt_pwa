<?php
//Permet de se connecter à la base de donnée
require 'connexionbdd.php'; 

if(isset($_POST['formconnect']))
{
    //Permet de sécurise pour éviter injonction html
    $pseudoconnect = htmlspecialchars($_POST['pseudoconnect']);
    $mdpconnect = sha1($_POST['mdpconnect']);
    //Regarde si le pseudo ainsi que le mdp soit bien remplie
    if(!empty($pseudoconnect) AND !empty($mdpconnect))
    {
        //Vas faire une requete pour savoir si le pseudo ainsi que le mot de passe existe bien
        $requser = $bdd->prepare("SELECT * FROM user WHERE pseudo = ? AND mot_de_passe = ?");
        $requser->execute(array($pseudoconnect, $mdpconnect));
        $userexist = $requser->RowCount();
        //Si l'utilisateur existe alors continue
        if($userexist == 1)
        {
            //Recupe l'ID, le pseudo et le mail de la personne si l'on veut l'afficher plus tard
            $userinfo = $requser->fetch();
            $_SESSION['ID'] = $userinfo['ID'];
            $_SESSION['pseudo'] = $userinfo['pseudo'];
            $_SESSION['mail'] = $userinfo['mail'];
            $_SESSION['id_role'] = $userinfo['id_role'];
            header("Location: index.php");

        }
        else
        {
            $erreur = "Pseudo ou mot de passe non valide !";
        }
    }
    else
    {
        $erreur = "Tous les champs doivent être remplie !";
    }
}
?>