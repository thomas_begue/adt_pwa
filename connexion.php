<!DOCTYPE html>
<html lang="fr">
<head>
<?php require 'connexionbdd.php' ?>
<?php session_start(); ?>
<meta charset="UTF-8">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
<?php require 'connect-site.php'; ?>
</head>
<body>
<main id="site-connexion">
<section class="banniere">

<?php require 'menu.php'; ?>


<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x align-center">
        <?php if(!empty($_SESSION['pseudo'])){
            
            echo "<h2 class='dejaconnect'>Vous ne pouvez pas vous connecter car vous êtes déjà connecté !<button><a href='deconnexion.php'>Se déconnecter</a></button>";
        }
        else
        { ?>
        <div class="cell small-12 medium-6 large-5  contour">
        <h2>Connexion</h2>
            <!-- Ici nous créons un formulaire pour permettre de l'utilisateur de se connecter -->
            <form method="POST" action="">
                <input type="text" name="pseudoconnect" placeholder="Pseudo" />
                <input type="password" name="mdpconnect" placeholder="Mot de passe" />
                <input class ="success button" type="submit" name="formconnect" value="Se connecter" />
            </form>
            <div class="Erreur">
                <?php 
                if(isset($erreur))
                {
                    echo $erreur;
                }
                ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
</section>
</main>
</body>
</html>