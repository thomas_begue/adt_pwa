<?php require_once 'templates-parts/header.php'; ?>

<div id="LesNews" class="container">
    <div class="contour">
        <div class="grid-x grid-margin-x grid-padding-x ">
            <div class="cell small-12">
                <h2>Les news</h2> 
            </div>
                <div class ="cell small-6 decale" align="center">
                    <div class="card" style="width: 30rem;">
                            <img src="assets/images/Trembl_terre.jpg" class="card-img-top" alt="Image tremblement de terre">
                                <div class="card-body">
                                    <p class="card-text">En raison du tremblement de terre qui à eu lieu au Japon le 14 mars 2021, la diffusion de l'épisode 73 de l'Attaque des Titans a été interrompue</p>
                                </div>
                    </div>
                </div>
                <div class ="cell small-6 decale" align="center" >
                    <div class="card" style="width: 30rem;">
                        <img src="assets/images/End.jpg" class="card-img-top" alt="Fin Attaque des titans">
                            <div class="card-body">
                                <p class="card-text">L'Attaque des Titans s'est fini le 09 avril 2021. Ce manga a bouleversé l'histoire du Shōnen (La cible est constituée principalement de jeunes adolescents de sexe masculin).</p>
                            </div>
                    </div>
                </div>           
                <div class ="cell small-6" align="center">
                    <div class="card" style="width: 30rem;">
                        <img src="assets/images/pika_edit.png" class="card-img-top" alt="Pika edition">
                            <div class="card-body">
                                <p class="card-text">La version française de l'Attaque des Titans est publiée par la société Pika Edition depuis Juin 2013.</p>
                            </div>
                    </div>
                </div>
                <div  class ="cell small-6" align="center">
                    <div class="card" style="width: 30rem;">
                        <img src="assets/images/Map_monde.jpg" class="card-img-top" alt="Image tremblement de terre">
                            <div class="card-body">
                                <p class="card-text">L'Attaque des Titans est un manga apprécié dans le monde entier. Il reçoit des critiques positives depuis sa création.</p>
                            </div>
                    </div>
                </div>
        </div> 
    </div>
</div>
<div class="container">
    <div class="contour">
        <div class="grid-x grid-margin-x grid-padding-x ">
            <div class="cell small-12">
                <h2>Synopsis</h2>
            </div>
            <div class="cell small-8 margin decale">
                <p>L'Attaque des Titans est un Shōnen manga écrit et dessiné par Hajime Isayama. Il est prépublié entre septembre 2009 et avril 2021. Il sera ensuite compilé en 34 volumes. L'histoire se base autour du personnage Eren Jäger dans un monde ou le peuple vit entourée d'immenses murs pour se protéger des créatures gigantesques appelées les Titans. En l’an 845, les humains n’ont pas aperçu de Titans aux abords du district de Shiganshina depuis plus d’un siècle. Le père d'Eren, Grisha Jäger, lui confie la clé de leur sous-sol, qui contiendrait des secrets capitaux. Malheureusement Eren et sa sœur adoptive, Mikasa, sont témoins de l’apparition d’un Titan de 60 mètres, le Titan colossal, ainsi que le Titan cuirassé, qui ouvrent une brèche dans le mur Maria. Eren n'a donc pas eu le temps de voir le secret de sa cave. Les Titans déferlent alors sur la ville et font un carnage, un titan souriant se précipite pour dévorer la mère d'Eren sous ses yeux. Celui-ci décide de prendre sa revanche et de tuer tous les Titans en intégrant le bataillon d’exploration.</p>
            </div>
            <div class="cell small-4 decale">
                <img src="assets/images/isayama.jpg">
            </div>
        </div>
    </div>
</div>

<div style="height=500px;">
<?php require_once 'templates-parts/footer.php'; ?>