<?php
session_start();
if(!empty($_SESSION['pseudo'])){
    if ($_SESSION['id_role'] == 1){
//Permet de se connecter à la base de donnée
require 'connexionbdd.php'; 
//Permet d'activer le code au moment ou l'on cliquer sur le bouton 'Je m'inscris'
if(isset($_POST['insertperso']))
{
    //Ici va vérifié si chaque input(Champ) est remplie
    if(!empty($_POST['nomperso']) AND !empty($_POST['description']))
    {
        //Vérifie si le personnage n'est pas déjà rentré.
        $nomperso = $_POST['nomperso'];
        $description = $_POST['description'];
        $img = "assets/images/personnages/".$_FILES['image']['name'];
        $existnom = $bdd->prepare("SELECT nom FROM hero WHERE nom = ?");
        $existnom->execute(array($nomperso));
        $persoexist = $existnom->RowCount();
        if($persoexist == 0)
        {
            //Permet d'ajouter dans la table Hero le Hero ainsi que l'image dans la table hero aussi

            
            $insert_perso = $bdd->prepare("INSERT INTO hero(nom, descrip, chemin) VALUES(?, ?, ?)");
            $insert_perso->execute(array($nomperso, $description, $img));
            $erreur = "Le personnage a été ajouté";
        }
        else
        {
            $erreur = "Le nom du personnage est déjà existant";
        }
    }
    else
    {
        $erreur = "Tous les champs ne sont pas remplis";
    }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
</head>
<body>
<main id="personnages">
<section class="banniere">

<?php require 'menu.php'; ?>

<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x align-center">
        <div class="cell small-12 medium-6 large-5  contour">
        <h2>Insertion personnage</h2>
            <!-- Ici nous créons un formulaire pour permettre de l'utilisateur de se connecter -->
            <form method="POST" action="" enctype="multipart/form-data">
                <input type="text" name="nomperso" placeholder="Nom du personnage" />
                <TEXTAREA type="text" name="description" placeholder="Description personnage" class="champtext"></TEXTAREA>
                <input type="file" name="image" placeholder="Image"/>
                <input class ="success button" type="submit" name="insertperso" value="Inscrire un personnage"/>
            </form>
            <div class="Erreur">
                <?php 
                if(isset($erreur))
                {
                    echo $erreur;
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
}
}
else
{
    echo "<script>alert(\"Vous n'êtes pas administrateur\")</script>";
}
?>
</section>
</main>
</body>
</html>