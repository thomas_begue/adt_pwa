<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x align-center">
        <div class="cell small-12 medium-8 large-12" >
            <ul class="vertical medium-horizontal menu" data-responsive-menu="accordion medium-dropdown">
                <li class="couleur"><a href="inscription.php">INSCRIPTION</a></li>
                <?php if(!empty($_SESSION['pseudo'])){ ?>
                <li class="couleur"><a href="deconnexion.php">DECONNEXION</a></li>
                <?php }else { ?>
                <li class="couleur"><a href="connexion.php">CONNEXION</a></li>
                <?php } ?>
                <li class="accueil couleur"><a href="index.php">ACCUEIL</a></li>
                <li class="couleur"><a href="brigades.php">BRIGADES</a></li>
                <li class="couleur"><a href="personnages.php">PERSONNAGES</a></li>
            </ul>
        </div>
    </div>
</div>