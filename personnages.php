<?php
session_start();
require 'connexionbdd.php'; 
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
</head>
<body>
<main id="personnages">
<section class="background">

<?php require 'menu.php'; ?>

<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x align-center">
        <div class="cell small-12 medium-12 large-12 decale">
            <h1>Les personnages de l'Attaque des Titans</h1>
                <?php if(!empty($_SESSION['pseudo'])){
                    if ($_SESSION['id_role'] == 1){?>
            <a href="insert_personnage.php" class="nounderline"><h4>Inserer un personnage</h4></a>
            <?php }}else{}?>
        </div>
    </div>
</div>


<!-- requete pour allez chercher les valeurs des personnages pour ensuite les affichés. -->
<div class="grid-container">
    <div class="grid-x grid-margin-x grid-padding-x">
        <?php
            $reponse = $bdd->query('SELECT nom, descrip, chemin FROM hero');
            while ($recup_perso = $reponse->fetch())
            {
        ?>
        <div class="cell small-4 contour" align="center">
            <h3><?= $recup_perso['nom']; ?></h3>
            <img  src="<?=$recup_perso['chemin'];?>" class="redim" alt="test"> 
            <p class="decale"><?= $recup_perso['descrip']; ?></p>
        </div>
<?php
}
?>
</div>
</div>

</section>
</main>
</body>
</html>